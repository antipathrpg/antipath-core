import Discord from 'discord.js'
import { promisedEvents } from '../util/promised-event'

class SystemBotClient {
    public client: Discord.Client = new Discord.Client();

    public async connectSystemBot() {
        const BOT_TOKEN = process.env.DISCORD_BOT_TOKEN
        this.client.login(BOT_TOKEN).catch(err => {
            err.message = `Error logging in system bot: ${err.message.toLowerCase()}`
            return Promise.reject(err)
          }),

          await promisedEvents.promisedEvent(this.client, 'ready')
    }
}

export { SystemBotClient }
