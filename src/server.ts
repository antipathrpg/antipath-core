// Import stuff as required
import * as dotenv from 'dotenv';

import { SystemBotClient } from './clients/SystemBotClient'

// Init tasks
dotenv.config()
process.on('unhandledRejection', err => {
    console.log(err)
    process.exit(1)
})

  
class Game {
    SystemBot: SystemBotClient = new SystemBotClient();

    // Run once: actions to take on server start
    async boot() {
        console.log("Starting up, please wait...")
        await this.SystemBot.connectSystemBot()
    }

}

// Boot up the game
const game = new Game();

new Promise((resolve, reject) => {
    resolve(game.boot())
}) 
    .then(() => {
        console.log(`Game started!`)
    })
    .catch(err => {
        console.log(err)
        process.exit(1)
    })
