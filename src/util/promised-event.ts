// Resolves once `ee` emits `evt`.
class promisedEvents {
  public static async promisedEvent(ee: any, evt: string) {
    await new Promise(resolve => {
      ee.once(evt, (...args: any) => resolve(...args))
    })
  } 
}

export {promisedEvents}
