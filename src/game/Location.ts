import { TextChannel, BufferResolvable }  from 'discord.js'

export default class Location {
    protected channel: TextChannel

    constructor(c: TextChannel) {
        this.channel = c
    }

    public async createWebhook(name: string, avatarPath: BufferResolvable) {
        return await this.channel.createWebhook(name, avatarPath)
    }
}
