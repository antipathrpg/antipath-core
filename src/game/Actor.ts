import { Webhook, RichEmbed, BufferResolvable } from 'discord.js'
import Location from './Location'

export default class Actor {
    protected location: Location
    private webhook: Webhook
    private _name: string
    private _icon: BufferResolvable

    public async init(loc: Location, name: string, icon: BufferResolvable) {
        this.location = loc
        this._name = name
        this._icon = icon

        this.webhook = await this.location.createWebhook(name, icon)
    }

    public async moveTo(loc: Location) {
        await this.webhook.delete()
        this.location = loc
        this.webhook = await this.location.createWebhook(this._name, this._icon)
    }

    public async name(setTo?: string) {
        if (setTo == undefined) {
            return this.webhook.edit(setTo, this._icon)
              .then(() => this._name = setTo)
        } else {
            return this._name;
        }
    }

    public async icon(setTo?: BufferResolvable) {
        if (setTo == undefined) {
            return this.webhook.edit(this._name, setTo)
              .then(() => this._icon = setTo)
        } else {
            return this._icon;
        }
    }

    public async say(message: string, embed?: RichEmbed) {
        if(embed == undefined) {
            await this.webhook.send(message)
        } else {
            await this.webhook.send(message, embed)
        }
    }

    public async destroy() {
        await this.webhook.delete()
    }

}
